# react-todolist mobx 적용 후 피드백

## 1) @action을 사용하는 이유?
일반적으로 observable값이 변경될때마다 렌더링이 수행되면 렌더링은 수없이 많이 실행될 것이다. 하지만 @action데커레이터를 사용함으로써 트랜잭션 단위로 업데이트를 처리하기에 성능상의 장점을 가지고 있다.
자세한 내용은 [여기](https://hyunseob.github.io/2017/10/07/hello-mobx/)를 참고하자.

추가적으로 [직방 기술 블로그](https://medium.com/zigbang/mobx-%EC%B5%9C%EC%A0%81%ED%99%94%EB%A1%9C-%EC%A7%81%EB%B0%A9%EC%95%B1-%EC%84%B1%EB%8A%A5-%EA%B0%9C%EC%84%A0%ED%95%98%EA%B8%B0-3cd6e1cec569)의 내용을 참고하면 더 오래 기억하기 좋다.

## 2) @computed를 사용하는 이유?
observable 값으로 부터 파생되는 값을 도출하기 위해 사용한다. 피드백 전 isAllChecked(전체 체크 버튼)의 상태를 별도의 observable로 구성했었다. 하지만 isAllChecked는 TodoItem들로부터 의존적이며 파생된 값이였다. 이는 별도로 observable 로 구성하기보단 computed로 구성하는게 더 적합하다.

## 3) observer를 사용하는 이유?
observable 값의 상태 변경을 통보받아 렌더링을 수행하기 위해 사용한다.

## 4) 조건부 스타일을 적용하는 다른 방법!
classnames/bind가 아닌 바로 classnames에 접근하여 사용한다. 그리고 less파일에 string형태로 접근하는게 아닌 component로 접근한다.(IDE의 지원에 따라 less 파일의 해당 부분으로 넘어갈 수 있는 장점을 가지고 있다.)

~~~
import React from "react";
import cn from "classnames";
import s from "./ClearCompletedBtn.module.less";

const ClearCompletedBtn = (props) => {
    const isShow =  props.todoStore.todos.filter((todo) => (todo.checked === true)).length > 0;
    const handleClearCompletedClick = () => { props.todoStore.deleteCheckedTodoItem(); }

    return (
        <button className={cn(s.clear-completed, {[s.show] : isShow})} type="button" onClick={handleClearCompletedClick}>Clear completed</button>
    );
};

export default ClearCompletedBtn;
~~~

## 5) 컴포넌트에 Store 적용 기준

![image](https://user-images.githubusercontent.com/44339530/137827725-443aaa6c-3830-403e-82a0-8a3e55d412f0.png)

컴포넌트들은 위의 그림과 같이 Store와 유기적으로 상호작용을 하며 렌더링을 수행한다. 하지만 전체 컴포넌트가 Store와 연관을 갖으며 계속 상호작용을 주고받는다면 렌더링 횟수가 엄청나게 많아질 것이다. 또한 관리측면에서도 좋지 못할 것이다.

기본적으로 똑똑한 컴포넌트와 멍청한 컴포넌트로 나눠서 생각해보자. 여기서 말하는 똑똑한 컴포넌트란 전체의 상태를 총괄할 수 있는 컴포넌트를 말하고, 멍청한 컴포넌트란 단순하게 props로 값만 전달받아 렌더링되는 컴포넌트를 말한다.

![image](https://user-images.githubusercontent.com/44339530/137827911-0bcfe849-897d-4efe-a979-68b518cfb82c.png)

위의 그림에서처럼 Store와 직접 상호작용하는 똑똑한 컴포넌트들만 Store로 유기적인 관계를 맺도록하고 나머지 멍청한 컴포넌트들은 단순하게 props로 값만 전달받아 렌더링되도록 하는게 이상적이다. 하지만 위의 기준처럼 설계하는데에는 경험과 많은 생각이 필요하다.

## 6) Store 설계 방법

![image](https://user-images.githubusercontent.com/44339530/137828355-a3d58bcf-3e4c-49d8-ba08-bbde0654d650.png)

피드백 전 싱글톤 형태로 전역으로 관리할 수 있는 Store들을 만들었다. 하지만 만약에 한 페이지에 별도의 투두리스트를 하나 더 추가한다고 생각해보자. 전역의 싱글톤 방식 Store로 인해 두 개의 투두리스트가 동기화 될 것이다.
이러한 문제를 해결하기 위해 컴포넌트 묶음별로 Store를 별도로 사용하는 것이다. 

![image](https://user-images.githubusercontent.com/44339530/137828647-7591d4ba-03c5-403a-a167-4e09b875302a.png)

위와 같은 이미지의 구조가 될 것이다. 이처럼 여러 Store들을 관리하며 Store를 외부에서 컴포넌트에 주입하기 위해 사용하는 것이다. Context이다. 이와 관련해서 [여기](https://ko.reactjs.org/docs/context.html)를 참고하자.
