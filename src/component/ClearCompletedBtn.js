import React from "react";
import classNames from "classnames/bind";
import styles from "./ClearCompletedBtn.module.less";

const cx = classNames.bind(styles);

const ClearCompletedBtn = (props) => {
    const isShow =  props.todoStore.todos.filter((todo) => (todo.checked === true)).length > 0;
    const handleClearCompletedClick = () => { props.todoStore.deleteCheckedTodoItem(); }

    return (
        <button className={cx('clear-completed', {show : isShow})} type="button" onClick={handleClearCompletedClick}>Clear completed</button>
    );
};

export default ClearCompletedBtn;