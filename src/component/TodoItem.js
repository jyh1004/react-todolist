import React from "react";
import classNames from "classnames/bind";
import styles from "./TodoItem.module.less";

const cx = classNames.bind(styles);

const TodoItem = (props) => {    

    const handleCheckedChange = () => {
        props.checkTodoItem(props.id);
    };

    const handleDeleteClick = () => {
        props.deleteTodoItem(props.id);
    }

    return (
        <li className={cx('todo-item')}>
            <input id={props.id} className={cx('toggle')} type="checkbox" checked={props.checked} onChange={handleCheckedChange}/>
            <label>{props.text}</label>
            <button className={cx('destory')} onClick={handleDeleteClick}>x</button>
        </li>
    );
};

export default TodoItem;