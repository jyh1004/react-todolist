import { createGlobalStyle } from 'styled-components';
import reset from 'styled-reset';

const GlobalStyle = createGlobalStyle`
    ${reset};
    body {
        font: 14px 'Helvetica Neue', Helvetica, Arial, sans-serif;
        line-height: 1.4em;
        font-weight: 300;
        background-color: #f5f5f5;
    };
    a {
        text-decoration: none;
    }
`;

export default GlobalStyle;