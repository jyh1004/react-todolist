import React from "react";
import classNames from "classnames/bind";
import styles from "./TodoInput.module.less";
import useStore from "./stores/useStore";

const cx = classNames.bind(styles);

const TodoInput = () => {
    const { todoStore } = useStore(); 

    const isEmptyText = (text) => (text.trim() === '');

    const onKeyPress = (event) => {
        if(event.key=='Enter') {
            const text = event.target.value;
            if(isEmptyText(text)) return;
            
            todoStore.addTodoItem(text.trim());
            event.target.value = '';
        }
    }

    return (
        <input id="todo-input" className={cx('todo-input')} placeholder="What needs to be done?" onKeyPress={onKeyPress} autoFocus />
    );
};

export default TodoInput;