import React from "react";
import classNames from "classnames/bind";
import styles from "./Aside.module.less";

const cx = classNames.bind(styles);

const AsideHeader = () => (
    <header>
        <h3>Javascript</h3>
        <div>Vanila Javascript Example</div>
        <a href="https://github.com/tastejs/todomvc/tree/gh-pages/examples/vanillajs">Source</a>
    </header>
);

const AsideContent = () => (
    <div className={cx('aside-content')}>
        <p>
            JavaScript® (often shortened to JS) 
            is a lightweight, interpreted, 
            object-oriented language with first-class functions, 
            most known as the scripting language for Web pages, 
            but used in many non-browser environments as well such as node.js 
            or Apache CouchDB.
        </p>

        <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript">Javascript</a>
    </div>
);

const AsideFooter = () => (
    <footer>
        <em>
            If you have other helpful links to share, 
            or find any of the links above no longer work, 
            please <a href="#">let us know</a>.
        </em>
    </footer>
);
 
const Aside = () => (
    <div className={cx('aside-wrap')}>
        <AsideHeader />
        <hr />
        <AsideContent />
        <hr />
        <AsideFooter />
    </div>
);

export default Aside;