import React from "react";
import classNames from "classnames/bind";
import styles from "./TodoItemList.module.less";
import useStore from "./stores/useStore";
import { observer } from "mobx-react";

import TodoItem from "./TodoItem";

const cx = classNames.bind(styles);

const TodoItemList = observer(() => {
    const { todoStore } = useStore();
    
    return (
        <ul className={cx('todo-list')}>
            {
                todoStore.filterTodos.map((todo) => (
                    <TodoItem
                        key={todo.id}
                        id={todo.id}
                        text={todo.text}
                        checked={todo.checked}
                        checkTodoItem={todoStore.checkTodoItem}
                        deleteTodoItem={todoStore.deleteTodoItem}
                    />
                ))
            }
        </ul>
    );
});

export default TodoItemList;