import React from "react";

import GlobalStyle from './styles/GlobalStyle';
import Aside from './Aside.js'
import TodoApp from './TodoApp.js'

const App = () => (
  <React.Fragment>
    <GlobalStyle />
    <Aside />
    <TodoApp />
  </React.Fragment>
);

export default App;
