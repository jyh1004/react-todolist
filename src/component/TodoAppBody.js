import React from "react";
import classNames from "classnames/bind";
import styles from "./TodoAppBody.module.less";

import AllCheckBtn from "./AllCheckBtn";
import TodoInput from "./TodoInput";
import TodoItemList from "./TodoItemList";
import TodoFooter from "./TodoFooter";

const cx = classNames.bind(styles);

const TodoAppBody = () => {
    return (
        <div className={cx('app-body')}>
            <AllCheckBtn />
            <TodoInput />
            <TodoItemList />
            <TodoFooter />
        </div>
    );
};

export default TodoAppBody;