import React from "react";
import classNames from "classnames/bind";
import styles from "./TodoFilters.module.less";
import useStore from "./stores/useStore";
import { observer } from "mobx-react";

const cx = classNames.bind(styles);

const TodoFilters = observer(() => {
    const { todoStore } = useStore();
    const isAllFilter = todoStore.selectedFilter === 'all';
    const isActiveFilter = todoStore.selectedFilter === 'active';
    const isCompleted = todoStore.selectedFilter === 'completed';

    return (
        <ul className={cx('filters')}>
            <li>
                <a className={cx({selected: isAllFilter})} onClick={() => (todoStore.changeFilter('all'))}>All</a>
            </li>
            <li>
                <a className={cx({selected: isActiveFilter})} onClick={() => (todoStore.changeFilter('active'))}>Active</a>
            </li>
            <li>
                <a className={cx({selected: isCompleted})} onClick={() => (todoStore.changeFilter('completed'))}>Completed</a>
            </li>
        </ul>
    );
});

export default TodoFilters;