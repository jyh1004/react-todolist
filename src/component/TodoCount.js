import React from "react";
import classNames from "classnames/bind";
import styles from "./TodoCount.module.less";

const cx = classNames.bind(styles);

const TodoCount = (props) => {
    return (
        <span className={cx('todo-count')}>
            <strong>{props.todoCount}</strong>
            items left
        </span>
    );
};

export default TodoCount;