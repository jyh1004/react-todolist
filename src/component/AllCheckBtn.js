import React from "react";
import classNames from "classnames/bind";
import styles from "./AllCheckBtn.module.less";
import useStore from "./stores/useStore";
import { observer } from "mobx-react";

const cx = classNames.bind(styles);

const AllCheckBtn = observer(() => {
    const { todoStore } = useStore(); 
    
    const onChange = () => { // AllCheckBtn 스스로만 보자면 onChange가 더 적합함 
        todoStore.checkAllTodoItem();
    }
 
    return (
        <div>
            <input id="toggle-all" className={cx('toggle-all')} type="checkbox" checked={todoStore.isAllChecked} onChange={onChange} />
            <label htmlFor="toggle-all"></label>
        </div>
    );
});

export default AllCheckBtn;