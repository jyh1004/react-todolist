import React from "react";
import classNames from "classnames/bind";
import styles from "./TodoApp.module.less";

import TodoAppFooter from "./TodoAppFooter";
import TodoAppBody from "./TodoAppBody";

const cx = classNames.bind(styles);

const TodoAppHeader = () => (
    <h1>todos</h1>
);

const TodoApp = () => {

    return (
        <section className={cx('todo-app')}>
            <TodoAppHeader />
            <TodoAppBody />
            <TodoAppFooter />
        </section>
    );
};

export default TodoApp;