import React from "react";
import classNames from "classnames/bind";
import styles from "./TodoAppFooter.module.less";

const cx = classNames.bind(styles);

const TodoAppFooter = () => {
    return (
        <footer className={cx('app-footer')}>
                <p>Double-click to edit a todo</p>
                <p>Created by <a href="http://twitter.com/oscargodson">Oscar Godson</a></p>
                <p>Refactored by <a href="https://github.com/cburgmer">Christoph Burgmer</a></p>
                <p>Part of <a href="http://todomvc.com">TodoMVC</a></p>
        </footer>
    );
};

export default TodoAppFooter;