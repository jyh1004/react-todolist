import { observable, action, computed } from 'mobx';
import uuid from "react-uuid"

class TodoStore {
    @observable todos = [];

    @observable selectedFilter = 'all'; // all, active, completed
    
    @computed get isAllChecked() { // 전체 체크된 상태: true, 하나 이상 체크 안된 상태: false
        const {todos} = this;
        if (todos.length === 0) {
            return false;
        }

        return todos.every((todo) => todo.checked);
    }

    @computed get calculateTodoCount() { // 잔여 Todo 항목 개수
        return this.todos.filter(todo => !todo.checked).length;
    };

    @computed get filterTodos() {
        return this.todos.filter((todo) => {

            if (this.selectedFilter === 'all') {
                return true;
            }
    
            if (this.selectedFilter === 'active' && !todo.checked) {
                return true;
            }
    
            if (this.selectedFilter === 'completed' && todo.checked) {
                return true;
            }
    
            return false;
        });
    };

    /**
     * todo 항목을 추가한다.
     * @param {string} text todo 항목 내용
     * @returns {void}
     */
    @action
    addTodoItem = (text) => {
        this.todos.push({
            id: uuid(),
            text: text,
            checked: false,
        });
    };

    /**
     * 해당 id와 일치하는 todo 항목을 삭제한다.
     * @param {number} id todo id
     * @returns {void}
     */
    @action
    deleteTodoItem = (id) => {
        const todoItem = this.todos.find((item) => (item['id'] === id));
        const idx = this.todos.indexOf(todoItem);
        this.todos.splice(idx, 1);
    };

    /**
     * 해당 id와 일치하는 todo 항목을 체크한다.
     * @param {number} id todo id
     * @returns {void}
     */
    @action
    checkTodoItem = (id) => {
        const todoItem = this.todos.find((item) => (item['id'] === id));
        todoItem.checked = !todoItem.checked;
    };

    /**
     * 선택된 항목들을 전체 삭제 한다.
     * @param {void}
     * @returns {void}
     */
    @action
    deleteCheckedTodoItem = () => {
        this.todos = this.todos.filter((todo) => !todo.checked);
    };

    /**
     * 전체 Todo항목의 체크 버튼을 클릭한다.
     * @param {void}
     * @returns {void}
     */
    @action
    checkAllTodoItem = () => {
        const isAllChecked = this.isAllChecked;
        this.todos.forEach((todo) => ( todo.checked = !isAllChecked ));
    };

    @action
    changeFilter = (filter) => {
        this.selectedFilter = filter;
    }
}

const todoStore = new TodoStore();

export default todoStore;