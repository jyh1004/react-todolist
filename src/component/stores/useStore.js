import todoStore from "./TodoStore";

const useStore = () => ({ 
    todoStore
});

export default useStore;
