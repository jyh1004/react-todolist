import React from "react";
import classNames from "classnames/bind";
import styles from "./TodoFooter.module.less";
import useStore from "./stores/useStore";
import { observer } from "mobx-react";

import TodoCount from "./TodoCount";
import TodoFilters from "./TodoFilters";
import ClearCompletedBtn from "./ClearCompletedBtn";

const cx = classNames.bind(styles);

const TodoFooter = observer(() => {
    const { todoStore } = useStore();
    const isShow = todoStore.todos.length > 0 ? true : false;

    return (
        <div className={cx('todo-footer', {show : isShow})}>
            <TodoCount todoCount={todoStore.calculateTodoCount}/>
            <TodoFilters />
            <ClearCompletedBtn todoStore={todoStore}/>
        </div>
    );
});

export default TodoFooter;