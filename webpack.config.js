const path = require('path'); // core nodejs 모듈 중 하나, 파일 경로 설정할 때 사용
const HtmlWebpackPlugin = require('html-webpack-plugin'); // script 태그를 사용하여 body에 모든 webpack 번들을 포함하는 HTML5 파일을 생성
const MiniCssExtractPlugin = require('mini-css-extract-plugin'); // CSS를 <style> 태그로 포함시키는게 아닌 별도의 파일로 분리
const isProduction = process.env.NODE_ENV === 'production';

module.exports = {
    entry: path.join(__dirname, 'src', 'index.js'), // 리액트 파일이 시작하는 곳
    output: {
        path: path.join(__dirname, '/dist'),
        filename: 'index_bundle.js',
    },
    module: {
        rules: [ // javascript 모듈을 생성할 규칙을 지정 (node_module을 제외한.js 파일을 babel-loader로 불러와 모듈을 생성)
            {
                test: /\.js$/, // .js, .jsx로 끝나는 babel이 컴파일하게 할 모든 파일 
                exclude: /node_module/, // node_module 폴더는 babel 컴파일에서 제외
                use: {
                    loader: 'babel-loader' // babel loader가 파이프를 통해 js 코드를 불러옴
                }
            },
            {
                test: /\.less$/,
                use: [
                    MiniCssExtractPlugin.loader, 
                    {loader: 'css-loader', options: {modules: true}}, 
                    'less-loader']
            },
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/index.html' // 생성한 템플릿 파일
        }),
        new MiniCssExtractPlugin({ filename: 'css/style.css' })
    ],
    devServer: {
        host: 'localhost', // host 설정
        port: 3000,// port 설정
        open: true, // 서버를 실행했을 때, 브라우저를 열어주는 여부
        compress: true,
        hot: true,
        historyApiFallback: true,
    },
    devtool: "eval-cheap-source-map" // TODO: 해당 옵션에 대해 더 자세히 찾아보기
}